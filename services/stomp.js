const stompit = require('stompit');

/**
 * stompit connection options.
 */
const options = {
  host: 'mq',
  port: 61613,
  connectHeaders: {
    host: '/',
    login: 'guest',
    passcode: 'guest',
    'heart-beat': '5000,5000',
  },
};
const maxReconnect = {
  maxReconnects: 1,
};

/**
 * The stompit client. Will be initialized upon first event emission.
 */
let client = null;

/**
 * Connect to the Stomp server.
 * Reconnect on errors.
 * @private
 *
 * @returns {Promise.<stompit.Client>}
 */
async function connect() {
  const manager = new stompit.ConnectFailover([options], maxReconnect);

  return new Promise((resolve, reject) => {
    console.log('[Stomp] client connecting...');
    manager.connect((connectionError, newClient, reconnect) => {
      if (connectionError) {
        console.log('[Stomp] connection error', connectionError);
        return reject(connectionError);
      }

      console.log('[Stomp] connection established');
      newClient.on('error', (clientError) => {
        console.log('[Stomp] client error', clientError);
        console.log('[Stomp] client reconnecting...');
        reconnect();
      });

      return resolve(newClient);
    });
  });
}

/**
 * Reconnect to the queue if necessary.
 */
async function initialize() {
  client = client || await connect();
}

/**
 * Send a frame on the given queue.
 * @private
 *
 * @param {String} queue
 * @param {Object} payload
 * @returns {Promise}
 */
async function sendFrame(queue, payload) {
  client = client || await connect();

  const headers = {
    destination: queue,
    'content-type': 'application/json',
  };

  return new Promise((resolve) => {
    const json = JSON.stringify(payload);

    const frame = client.send(headers, {
      /**
       * Called when the job has been received by the MQ.
       * @returns {*}
       */
      onReceipt() {
        console.log(`[Stomp] message acknowledged by MQ: ${json}`);
        return resolve();
      },
    });

    console.log(`[Stomp] sending payload: ${json}`);
    frame.write(json);

    frame.end();
  });
}

/**
 * Emit a payload on the given queue.
 * @param {String} queue
 * @param {Object} payload
 * @param {Number} maxAttempts
 * @returns {Promise}
 */
async function send(queue, payload, maxAttempts = 1) {
  return sendFrame(queue, {
    origin: options.origin,
    attempt: 1,
    maxAttempts,
    timestamp: +new Date(),
    payload,
  });
}

/**
 * Subscribe to messages on the given queue.
 * @param {String} queue
 * @param {Function} onMessage
 */
async function subscribe(queue, onMessage) {
  client = client || await connect();

  // https://stomp.github.io/stomp-specification-1.2.html#SUBSCRIBE_ack_Header
  const headers = {
    destination: queue,
    ack: 'client-individual',
  };

  console.log(`[Stomp] subscribing to queue ${queue}...`);
  client.subscribe(headers, (subscriptionError, message) => {
    if (subscriptionError) {
      console.log('[Stomp] subscription error', subscriptionError);
      return;
    }

    console.log(`[Stomp] subscribed to queue ${queue}`);
    message.readString('utf-8', (readError, json) => {
      if (readError) {
        console.log('[Stomp] read error', readError);
        return;
      }

      const data = JSON.parse(json);

      Promise.resolve(onMessage(data.payload))
        .then(() => {
          client.ack(message);
        })
        .catch(() => {
          if (data.attempt === data.maxAttempts) {
            // All attempts failed - give up and push job to failed queue.
            console.log('[Stomp] giving up on job');
            client.ack(message);
            sendFrame(`${queue}_failed`, data);
          } else {
            console.log('[Stomp] job failed - pushing back to queue');
            // Increment attempts count and push job back in queue.
            data.attempt += 1;
            sendFrame(queue, data);
          }
        });
    });
  });
}

module.exports = {
  initialize,
  send,
  subscribe,
};
