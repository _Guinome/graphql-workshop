const express = require('express');

const router = express.Router();

const { WebClient } = require('@slack/client');
const SLACK_ACCESS_TOKEN = 'xoxp-33495944082-33593685009-462986342147-35fe410585bed7a139560c58e30236ed';

/* GET home page. */
router.get('/', (req, res) => {
  res.render('index', { title: 'Express' });
});
router.get('/change-dog-breed', (req, res) => {
  //[dog_id,breed_id]
  // const ids = req.body.payloads;
  console.log(req.body);
});
router.get('/slack', (req, res) => {
  console.log(req.body);
  // update dog with breed asked
  const web = new WebClient(SLACK_ACCESS_TOKEN);
  // The current date
  // const currentTime = new Date().toTimeString();


  // This argument can be a channel ID, a DM ID, a MPDM ID, or a group ID
  const conversationId = 'CDNEK02R4';

  // See: https://api.slack.com/methods/chat.postMessage
  // const message = {
  //   channel: conversationId,
  //   text: 'Hello there',
  //   attachments: [
  //     {
  //       fallback: 'Required plain-text summary of the attachment.',
  //       color: '#36a64f',
  //       author_name: 'Bobby Tables',
  //       author_link: 'http://flickr.com/bobby/',
  //       author_icon: 'http://flickr.com/icons/bobby.jpg',
  //       title: 'Slack API Documentation',
  //       title_link: 'https://api.slack.com/',
  //       text: 'Optional text that appears within the attachment',
  //       fields: [
  //         {
  //           title: 'Priority',
  //           value: 'High',
  //           short: false,
  //         }
  //       ],
  //       image_url: 'http://my-website.com/path/to/image.jpg',
  //       thumb_url: 'http://example.com/path/to/thumb.png',
  //       footer: 'Slack API',
  //       footer_icon: 'https://platform.slack-edge.com/img/default_application_icon.png',
  //       ts: 123456789,
  //     },
  //   ],
  // };
  const message = {
    channel: conversationId,
    text: 'Would you like to play a game?',
    attachments: [
      {
        text: 'Choose a game to play',
        fallback: 'You are unable to choose a game',
        callback_id: 'wopr_game',
        color: '#3AA3E3',
        attachment_type: 'default',
        actions: [
          {
            name: 'game',
            text: 'Chess',
            type: 'button',
            value: 'chess',
            url: 'http://localhost/change-dog-breed',
          },
          {
            name: 'game',
            text: 'Falken\'s Maze',
            type: 'button',
            value: 'maze',
            url: 'http://localhost/change-dog-breed',
          },
          {
            name: 'game',
            text: 'Thermonuclear War',
            style: 'danger',
            type: 'button',
            value: 'war',
            url: 'http://localhost/change-dog-breed',
            confirm: {
              title: 'Are you sure?',
              text: "Wouldn't you prefer a good game of chess?",
              ok_text: 'Yes',
              dismiss_text: 'No',
            },
          },
        ],
      },
      // {
      //   callback_id: 'tender_button',
      //   attachment_type: 'default',
      //   actions: [
      //     {
      //       name: 'press',
      //       text: 'Press',
      //       type: 'button',
      //       value: 'pressed',
      //     },
      //   ],
      // },
    ],
  };
  web.chat.postMessage(message)
    .then((messageRes) => {
      // `messageRes` contains information about the posted message
      console.log('Message sent: ', messageRes.ts);
      console.log(messageRes);
    })
    .catch((error) => {
      console.log(error);
    });
  // return a message
  res.render('index', { title: 'Express' });
});

module.exports = router;
