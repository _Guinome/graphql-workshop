GraphQL Workshop https://lab.hellocomet.co/snippets/50

 - Get code `git clone`
 - Build & up containers `docker-compose build` then `docker-compose up -d`
 - Optional `docker-compose logs -f`
 - Populate db `docker exec -d graphql-workshop_db_1 psql -U postgres -d comet_dogs -a -f comet_dogs.sql`
 - Finally go to `localhost/graphql`