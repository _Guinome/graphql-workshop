
module.exports = (sequelize, DataTypes) => {
  const dog = sequelize.define('dog', {
    dog_id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    breed_id: DataTypes.INTEGER,
    photo_url: DataTypes.STRING,
    dog_name: DataTypes.STRING,
    dog_description: DataTypes.TEXT,
  }, {
    underscored: true,
  });
  dog.associate = (models) => {
    // associations can be defined here
    dog.belongsTo(models.breed, {
      foreignKey: 'breed_id',
      as: 'breed',
      constraints: false,
    });
  };
  return dog;
};
