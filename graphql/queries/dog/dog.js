const {
  GraphQLInt,
  GraphQLNonNull,
} = require('graphql');

const models = require('../../../models/index.js');
const GraphQLDog = require('../../types/dog.js');

module.exports = {
  type: GraphQLDog,
  args: {
    dog_id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
  },
  resolve(root, args) {
    return models.dog.findById(args.dog_id, {
      include: [{
        model: models.breed,
        as: 'breed',
      }],
    });
  },
};
