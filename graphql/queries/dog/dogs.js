const {
  GraphQLList,
  GraphQLString,
} = require('graphql');

const models = require('../../../models/index.js');
const GraphQLDog = require('../../types/dog.js');

module.exports = {
  type: new GraphQLList(GraphQLDog),
  args: {
    search: {
      type: GraphQLString,
    },
  },
  // args: {
  //   breed_id: {
  //     type: GraphQLInt,
  //   },
  //   first: {
  //     type: GraphQLInt,
  //     description: 'Limits the number of results returned in the page. Defaults to 10.',
  //   },
  //   offset: {
  //     type: GraphQLInt,
  //   },
  // },
  resolve(root, args) {
    // const offset = args.offset || 0;
    // const limit = args.first || 10;
    // delete args.offset;
    // delete args.first;
    // console.log(args.search);
    if (
      args.search !== ''
      && typeof args.search === 'string'
    ) {
      return models.dog.findAll({
        include: [{
          model: models.breed,
          as: 'breed',
        }],
        where: {
          dog_name: {
            $like: `%${args.search}%`,
          },
        },
      });
    }
    return models.dog.findAll({
      include: [{
        model: models.breed,
        as: 'breed',
      }],
    });
  },
};
