const breed = require('./breed/breed.js');
const breeds = require('./breed/breeds.js');

const dog = require('./dog/dog.js');
const dogs = require('./dog/dogs.js');

module.exports = {
  breed,
  breeds,
  dog,
  dogs,
};
