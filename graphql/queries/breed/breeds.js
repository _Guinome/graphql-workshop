const {
  GraphQLList,
} = require('graphql');

const models = require('../../../models/index.js');
const GraphQLBreed = require('../../types/breed.js');

module.exports = {
  type: new GraphQLList(GraphQLBreed),
  resolve() {
    return models.breed.findAll({
      include: [{
        model: models.dog,
        as: 'dogs',
      }],
    });
  },
};
