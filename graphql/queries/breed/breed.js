const {
  GraphQLInt,
  GraphQLNonNull,
} = require('graphql');

const models = require('../../../models/index.js');
const GraphQLBreed = require('../../types/breed.js');

module.exports = {
  type: GraphQLBreed,
  args: {
    breed_id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
  },
  resolve(root, args) {
    return models.breed.findById(args.breed_id, {
      include: [{
        model: models.dog,
        as: 'dogs',
      }],
    });
  },
};
