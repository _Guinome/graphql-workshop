const models = require('../../../models/index.js');
const GraphQLBreed = require('../../types/breed.js');
const BreedInput = require('../../inputs/breed.js');

module.exports = {
  type: GraphQLBreed,
  args: {
    breed: {
      type: BreedInput,
    },
  },
  resolve(source, args) {
    // need to check all fields...
    const fieldsToCreate = {};
    Object.entries(args.breed).forEach(([key, value]) => {
      if (value !== null || value !== '') {
        fieldsToCreate[key] = value;
      }
    });
    return models.breed.build(fieldsToCreate).save()
      .then(newBreed => models.breed.findById(newBreed.breed_id));
  },
};
