const {
  GraphQLInt,
  GraphQLNonNull,
} = require('graphql');
const models = require('../../../models/index.js');
const Breed = require('../../types/breed.js');

module.exports = {
  type: Breed,
  args: {
    breed_id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
  },
  resolve(source, args) {
    return models.breed
      .findById(args.breed_id)
      .then(breed => breed.destroy({ force: true }));
  },
};
