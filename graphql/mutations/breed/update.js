const {
  GraphQLInt,
  GraphQLNonNull,
} = require('graphql');
const models = require('../../../models/index.js');
const Breed = require('../../types/breed.js');
const BreedInput = require('../../inputs/breed.js');

module.exports = {
  type: Breed,
  args: {
    breed_id: {
      type: new GraphQLNonNull(GraphQLInt),
    },
    breed: {
      type: BreedInput,
    },
  },
  resolve(source, args) {
    // need to check all fields...
    const fieldsToUpdate = {};
    Object.entries(args.breed).forEach(([key, value]) => {
      if (value !== null || value !== '') {
        fieldsToUpdate[key] = value;
      }
    });
    return models.breed
      .findById(args.breed_id)
      .then(breed => breed.update(fieldsToUpdate));
  },
};
