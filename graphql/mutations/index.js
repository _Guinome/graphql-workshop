const createBreed = require('./breed/create.js');
const updateBreed = require('./breed/update.js');
const deleteBreed = require('./breed/delete.js');

const createDog = require('./dog/create.js');
const updateDog = require('./dog/update.js');
const deleteDog = require('./dog/delete.js');

module.exports = {
  createBreed,
  updateBreed,
  createDog,
  updateDog,
  deleteDog,
  deleteBreed,
};
