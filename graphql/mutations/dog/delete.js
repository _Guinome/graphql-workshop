const {
  GraphQLInt,
  GraphQLNonNull,
} = require('graphql');
const models = require('../../../models/index.js');
const GraphQLDog = require('../../types/dog.js');
const DogInput = require('../../inputs/dog.js');

module.exports = {
  type: GraphQLDog,
  args: {
    dog_id: {
      type: new GraphQLNonNull(GraphQLInt)
    }
  },
  resolve (source, args) {
    return models.dog
      .findById(args.dog_id)
      .then( async (dog) => {
        await dog.destroy({ force: true });
        return dog.id;
      });
  }
};
