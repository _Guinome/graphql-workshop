const {
  GraphQLInt,
  GraphQLNonNull,
} = require('graphql');
const models = require('../../../models/index.js');
const GraphQLDog = require('../../types/dog.js');
const DogInput = require('../../inputs/dog.js');

module.exports = {
  type: GraphQLDog,
  args: {
    dog_id: {
      type: new GraphQLNonNull(GraphQLInt)
    },
    dog: {
      type: DogInput
    }
  },
  resolve (source, args) {
    const fieldsToUpdate = {};
    Object.entries(args.dog).forEach(([key, value]) => {
      if(value !== null || value !== ''){
        fieldsToUpdate[key] = value;
      }
    });
    return models.dog
      .findById(args.dog_id)
      .then((dog) => {
        return dog.update(fieldsToUpdate);
      });
  }
};
