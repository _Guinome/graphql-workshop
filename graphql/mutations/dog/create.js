const models = require('../../../models/index.js');
const GraphQLDog = require('../../types/dog.js');
const DogInput = require('../../inputs/dog.js');
const mq = require('../../../services/stomp.js');

module.exports = {
  type: GraphQLDog,
  args: {
    dog: {
      type: DogInput,
    },
  },
  resolve(source, args) {
    const fieldsToCreate = {};
    Object.entries(args.dog).forEach(([key, value]) => {
      if (value !== null || value !== '') {
        fieldsToCreate[key] = value;
      }
    });
    // need to check all fields...
    return models.dog
      .build(fieldsToCreate)
      .save()
      .then((newDog) => {
        mq.send('queue', 'DOG ADDED !!!').then(() => {
          console.log('Message dog added sent to "queue" 👌');
        });
        // mq.subscribe('queue').then(() => {
        //   console.log('subscription ready 👌');
        // });
        return models.dog.findById(newDog.dog_id);
      });
  },
};
