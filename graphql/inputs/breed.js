const {
  GraphQLInputObjectType,
  GraphQLString,
  GraphQLList,
} = require('graphql');

// const DogInput = require('./dog.js');

module.exports = new GraphQLInputObjectType({
  name: 'BreedInput',
  fields: () => ({
    breed_name: { type: GraphQLString },
    /* eslint-disable global-require */
    dogs: { type: new GraphQLList(require('./dog.js')) },
    /* eslint-enable global-require */
  }),
});
