const {
  GraphQLInputObjectType,
  GraphQLString,
  GraphQLInt,
} = require('graphql');

// const BreedInput = require('./breed.js');

module.exports = new GraphQLInputObjectType({
  name: 'DogInput',
  fields: () => ({
    breed_id: { type: GraphQLInt },
    dog_name: { type: GraphQLString },
    dog_description: { type: GraphQLString },
    photo_url: { type: GraphQLString },
    /* eslint-disable global-require */
    breed: { type: require('./breed.js') },
    /* eslint-enable global-require */
  }),
});
