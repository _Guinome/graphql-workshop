const {
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLString,
  GraphQLInt,
  GraphQLList,
} = require('graphql');

// const GraphQLDog = require('./dog.js');
const models = require('../../models/index.js');

module.exports = new GraphQLObjectType({
  name: 'breed',
  description: 'breed',
  fields: () => ({
    breed_id: {
      type: new GraphQLNonNull(GraphQLInt),
      description: 'breed id',
      resolve(breed) {
        return breed.breed_id;
      },
    },
    breed_name: {
      type: new GraphQLNonNull(GraphQLString),
      description: 'name of this breed',
      resolve(breed) {
        return breed.breed_name;
      },
    },
    created_at: {
      type: new GraphQLNonNull(GraphQLString),
      description: 'creation date of this breed',
      resolve(breed) {
        return breed.created_at;
      },
    },
    updated_at: {
      type: new GraphQLNonNull(GraphQLString),
      description: 'update date of this breed',
      resolve(breed) {
        return breed.updated_at;
      },
    },
    dogs: {
      type: new GraphQLList(require('./dog.js')),
      description: 'dogs',
      resolve: (breed) => {
        if (breed.hasOwnProperty('dogs')) {
          return breed.dogs;
        }
        return models.dog.findAll({ where: { breed_id: breed.breed_id } });
      },
    },
  }),
});
