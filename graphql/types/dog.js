const {
  GraphQLObjectType, GraphQLNonNull, GraphQLString, GraphQLInt,
} = require('graphql');

// const { GraphQLBreed } = require('./breed.js');
const models = require('../../models/index.js');

module.exports = new GraphQLObjectType({
  name: 'dog',
  description: 'dog',
  fields: () => ({
    dog_id: {
      type: new GraphQLNonNull(GraphQLInt),
      description: 'dog id',
      resolve(dog) {
        return dog.dog_id;
      },
    },
    breed_id: {
      type: new GraphQLNonNull(GraphQLInt),
      description: 'breed id',
      resolve(dog) {
        return dog.breed_id;
      },
    },
    dog_name: {
      type: GraphQLString,
      description: 'name of the dog',
      resolve(dog) {
        return dog.dog_name;
      },
    },
    dog_description: {
      type: GraphQLString,
      description: 'description of the dog',
      resolve(dog) {
        return dog.dog_description;
      },
    },
    photo_url: {
      type: GraphQLString,
      description: 'photo url of the dog',
      resolve(dog) {
        return dog.photo_url;
      },
    },
    created_at: {
      type: GraphQLString,
      description: 'creation date of this dog',
      resolve(dog) {
        return dog.created_at;
      },
    },
    updated_at: {
      type: GraphQLString,
      description: 'update date of this dog',
      resolve(dog) {
        return dog.updated_at;
      },
    },
    breed: {
      type: require('./breed.js'),
      description: 'breed',
      resolve: (dog) => {
        if (dog.hasOwnProperty('breed')) {
          return dog.breed;
        }
        return models.breed.findById(dog.breed_id);
      },
    },
  }),
});
