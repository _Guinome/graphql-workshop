
module.exports = {
  up: (queryInterface, Sequelize) => {
    queryInterface.createTable('dogs', {
      dog_id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      breed_id: {
        allowNull: false,
        type: Sequelize.INTEGER,
        references: {
          model: 'breeds', // name of Target model
          key: 'breed_id', // key in Target model that we're referencing
        },
        onUpdate: 'cascade',
        onDelete: 'cascade',
      },
      photo_url: {
        type: Sequelize.STRING,
      },
      dog_name: {
        type: Sequelize.STRING,
      },
      dog_description: {
        type: Sequelize.TEXT,
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },


  down: queryInterface => queryInterface.dropTable('dogs'),
};
