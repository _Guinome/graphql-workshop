const ret = `mutation {
    createDog(dog:{
      dog_name: "Yoyoyo"
      dog_description:"A yoyo dog"
      photo_url: "http://my.awesomepicture.dog"
      breed_id: 2
    }) {
      dog_id
      dog_name
      dog_description
      photo_url
      breed_id
    }
  }`;
// console.log(ret);
module.export = ret;
