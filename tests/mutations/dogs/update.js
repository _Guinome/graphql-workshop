module.export = (newDogId) => {
  const ret = {
    query: `mutation {
      updateDog(dog_id:${newDogId},dog:{
        dog_name: "yopyop"
        dog_description:"A yoyo dog"
        photo_url: "http://my.awesomepicture.dog"
        breed_id: 2
      }) {
        dog_id
        dog_name
        dog_description
        photo_url
        breed_id
      }
    }`,
  };
  return ret;
};
