const { tester } = require('graphql-tester');
// const models = require('../models/index.js');

const singleDogReturn = {
  dog: {
    dog_name: 'Yoyoyo',
    dog_description: 'A yoyo dog',
    photo_url: 'http://my.awesomepicture.dog',
    breed_id: 2,
  },
};
const updatedSingleDogReturn = {
  dog: {
    dog_name: 'yopyop',
    dog_description: 'A yoyo dog updated',
    photo_url: 'http://my.awesomepicture.dog/mypicture',
    breed_id: 3,
  },
};

const SingleDogCreateMutation = `mutation {
  createDog(dog:{
    dog_name: "Yoyoyo"
    dog_description:"A yoyo dog"
    photo_url: "http://my.awesomepicture.dog"
    breed_id: 2
  }) {
    dog_id
    dog_name
    dog_description
    photo_url
    breed_id
  }
}`;
// const { singleDogQuery } = require('./queries/dogs/dog.js');
const singleDogQuery = (newDogId) => {
  const ret = `{ 
      dog(dog_id: ${newDogId}) { 
        dog_id
        dog_name 
        dog_description 
        photo_url 
        breed_id 
      } 
    }`;
  return ret;
};
// const SingleDogCreateMutation = require('./mutations/dogs/create.js');
const dogsQuery = `{
  dogs {
    dog_id
    dog_name
    dog_description
    photo_url
    breed_id
  }
}`;
const dogsQueryWithSearch = `{
  dogs(search:"Yoyoyo") {
    dog_id
    dog_name
    dog_description
    photo_url
    breed_id
  }
}`;
// const { singleDogUpdateMutation } = require('./mutations/dogs/update.js');
const singleDogUpdateMutation = (newDogId) => {
  const ret = `mutation {
      updateDog(dog_id:${newDogId},
        dog:{
          dog_name: "yopyop",
          dog_description: "A yoyo dog updated",
          photo_url: "http://my.awesomepicture.dog/mypicture",
          breed_id: 3,
        }
      ) {
        dog_id
        dog_name
        dog_description
        photo_url
        breed_id
      }
    }`;
  return ret;
};

const singleDogDeleteMutation = (newDogId) => {
  const ret = `mutation {
      deleteDog(dog_id:${newDogId}) {
        dog_id
        dog_name
        dog_description
        photo_url
        breed_id
      }
    }`;
  return ret;
};

describe('Dog test', async () => {
  const self = this;
  const newDog = {};
  beforeAll(() => {
    self.test = tester({
      url: 'http://127.0.0.1/graphql',
      contentType: 'application/json',
    });
  });
  it('query dogs', (done) => {
    self
      .test(
        JSON.stringify({ query: dogsQuery }),
      )
      .then((res) => {
        expect(res.status).toBe(200);

        // check if each dog has every asked properties
        res.data.dogs.forEach((dog) => {
          expect(Object.prototype.hasOwnProperty.call(dog, 'dog_id')).toBe(true);
          expect(Object.prototype.hasOwnProperty.call(dog, 'dog_name')).toBe(true);
          expect(Object.prototype.hasOwnProperty.call(dog, 'dog_description')).toBe(true);
          expect(Object.prototype.hasOwnProperty.call(dog, 'photo_url')).toBe(true);
          expect(Object.prototype.hasOwnProperty.call(dog, 'breed_id')).toBe(true);
        });
        done();
      })
      .catch((err) => {
        expect(err).toBe(null);
        done();
      });
  });
  it('mutation createDog', (done) => {
    self
      .test(
        JSON.stringify({ query: SingleDogCreateMutation }),
      )
      .then((res) => {
        singleDogReturn.dog.dog_id = res.data.createDog.dog_id;
        updatedSingleDogReturn.dog.dog_id = res.data.createDog.dog_id;
        expect(res.status).toBe(200);
        expect(res.data.createDog).toEqual(singleDogReturn.dog);
        done();
      })
      .catch((err) => {
        expect(err).toBe(null);
        done();
      });
  });
  it('query dogs(search)', (done) => {
    self
      .test(
        JSON.stringify({ query: dogsQueryWithSearch }),
      )
      .then((res) => {
        expect(res.status).toBe(200);
        expect(res.data.dogs[0]).toEqual(singleDogReturn.dog);
        done();
      })
      .catch((err) => {
        expect(err).toBe(null);
        done();
      });
  });
  it('query dog', (done) => {
    self
      .test(
        JSON.stringify({ query: singleDogQuery(singleDogReturn.dog.dog_id) }),
      )
      .then((res) => {
        expect(res.status).toBe(200);
        expect(res.data).toEqual(singleDogReturn);
        done();
      })
      .catch((err) => {
        expect(err).toBe(null);
        done();
      });
  });
  it('mutation updateDog', (done) => {
    self
      .test(
        JSON.stringify({ query: singleDogUpdateMutation(updatedSingleDogReturn.dog.dog_id) }),
      )
      .then((res) => {
        expect(res.status).toBe(200);
        expect(res.data.updateDog).toEqual(updatedSingleDogReturn.dog);
        done();
      })
      .catch((err) => {
        expect(err).toBe(null);
        done();
      });
  });
  it('mutation deleteDog', (done) => {
    self
      .test(
        JSON.stringify({ query: singleDogDeleteMutation(singleDogReturn.dog.dog_id) }),
      )
      .then((res) => {
        expect(res.status).toBe(200);
        expect(res.data.deleteDog).toEqual(null);
        done();
      })
      .catch((err) => {
        expect(err).toBe(null);
        done();
      });
  });
});
