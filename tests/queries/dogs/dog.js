module.export = (newDogId) => {
  const ret = {
    query: `{ 
      dog(dog_id: ${newDogId}) { 
        dog_name 
        dog_description 
        photo_url 
        breed_id 
      } 
    }`,
  };
  return ret;
};
