FROM node:8
RUN mkdir /graphql-workshop
RUN npm install -g nodemon
RUN npm install -g sequelize-cli
COPY ./ /graphql-workshop
WORKDIR /graphql-workshop
RUN npm i
EXPOSE 80
CMD ["npm", "start"]