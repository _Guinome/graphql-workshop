const { WebClient } = require('@slack/client');

const SLACK_ACCESS_TOKEN = 'xoxp-33495944082-33593685009-462986342147-35fe410585bed7a139560c58e30236ed';

/**
 * 
 */
async function postSlackMessage() {
  // Create a new instance of the WebClient class with the token stored in your environment variable
  const web = new WebClient(SLACK_ACCESS_TOKEN);
  // The current date
  // const currentTime = new Date().toTimeString();


  // This argument can be a channel ID, a DM ID, a MPDM ID, or a group ID
  const conversationId = 'CDNEK02R4';
    
  // See: https://api.slack.com/methods/chat.postMessage
  // const message = {
  //   channel: conversationId,
  //   text: 'Hello there',
  //   attachments: [
  //     {
  //       fallback: 'Required plain-text summary of the attachment.',
  //       color: '#36a64f',
  //       author_name: 'Bobby Tables',
  //       author_link: 'http://flickr.com/bobby/',
  //       author_icon: 'http://flickr.com/icons/bobby.jpg',
  //       title: 'Slack API Documentation',
  //       title_link: 'https://api.slack.com/',
  //       text: 'Optional text that appears within the attachment',
  //       fields: [
  //         {
  //           title: 'Priority',
  //           value: 'High',
  //           short: false,
  //         }
  //       ],
  //       image_url: 'http://my-website.com/path/to/image.jpg',
  //       thumb_url: 'http://example.com/path/to/thumb.png',
  //       footer: 'Slack API',
  //       footer_icon: 'https://platform.slack-edge.com/img/default_application_icon.png',
  //       ts: 123456789,
  //     },
  //   ],
  // };
  const message = {
    text: 'Would you like to play a game?',
    attachments: [
      {
        text: 'Choose a game to play',
        fallback: 'You are unable to choose a game',
        callback_id: 'wopr_game',
        color: '#3AA3E3',
        attachment_type: 'default',
        actions: [
          {
            name: 'game',
            text: 'Chess',
            type: 'button',
            value: 'chess',
          },
          {
            name: 'game',
            text: 'Falken\'s Maze',
            type: 'button',
            value: 'maze',
          },
          {
            name: 'game',
            text: 'Thermonuclear War',
            style: 'danger',
            type: 'button',
            value: 'war',
            confirm: {
              title: 'Are you sure?',
              text: "Wouldn't you prefer a good game of chess?",
              ok_text: 'Yes',
              dismiss_text: 'No',
            }
          }
        ]
      }
    ]
  };
  return web.chat.postMessage(message)
    .then((res) => {
      // `res` contains information about the posted message
      console.log('Message sent: ', res.ts);
    })
    .catch(console.log('error'));
};

module.export = {
  postSlackMessage,
};
